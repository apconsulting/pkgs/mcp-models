package models

import "time"

type GLLink struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	URL       string `json:"url"`
	DirectURL string `json:"direct_asset_url"`
	External  bool   `json:"external"`
	LinkType  string `json:"link_type"`
}

// Release su gitlab
type GLRelease struct {
	Name        string    `json:"name"`
	Tag         string    `json:"tag_name"`
	Description string    `json:"description"`
	CreateDate  time.Time `json:"created_at"`
	ReleaseDate time.Time `json:"released_at"`
	Upcoming    bool      `json:"upcoming_release"`

	// Autore della release
	Author struct {
		ID       int    `json:"id"`
		Username string `json:"username"`
		Name     string `json:"name"`
		State    string `json:"state"`
		// AvatarURL
		// WebURL
	}

	// Commit legato alla release
	Commit struct {
		ID        string    `json:"id"`
		ShortID   string    `json:"short_id"`
		CreatedAt time.Time `json:"created_at"`
		ParentIDs []string  `json:"parent_ids"`

		Title          string    `json:"title"`
		Message        string    `json:"message"`
		AuthorName     string    `json:"author_name"`
		AuthorEmail    string    `json:"author_email"`
		AuthoredDate   time.Time `json:"authored_date"`
		CommitterName  string    `json:"committer_name"`
		CommitterEmail string    `json:"committer_email"`
		CommittedDate  time.Time `json:"committed_date"`
		WebURL         string    `json:"web_url"`
	} `json:"commit"`

	CommitPath string `json:"commit_path"`
	TagPath    string `json:"tag_path"`

	// Collezione di asset legati alla release (file linkati + sorgenti)
	Assets struct {
		Count int `json:"count"`

		// Sorgenti
		Sources []struct {
			Format string `json:"format"`
			URL    string `json:"url"`
		} `json:"sources"`

		// File
		Links []GLLink `json:"links"`
	} `json:"assets"`

	// ????
	Evidences []struct {
		Sha           string    `json:"sha"`
		FilePath      string    `json:"filepath"`
		CollectedDate time.Time `json:"collected_at"`
	} `json:"evidences"`
}
