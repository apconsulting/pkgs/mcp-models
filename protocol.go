package models

import "gitlab.com/apconsulting/pkgs/version"

type MCPResp struct {
	UpgVersion string `json:"upg-version"`
}

type ClientPayload struct {
	ClientInfo
}

// Pacchetto di informazioni comunicato dal processo al Sark
type InfoPacket struct {
	// Descrizione estesa del programma in esecuzione
	Description string

	// Versione del programma
	VersionTag version.Versione

	// Varie
	Vars map[string]any
}

type JwtClaims struct {
	Client   string `json:"client"`
	Software string `json:"software"`
}
