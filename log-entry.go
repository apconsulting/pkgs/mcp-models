package models

import (
	"strings"
	"time"
)

type LogLevel int64

const (
	TRACE LogLevel = iota + 1
	DEBUG
	INFO
	WARN
	ERROR
	FATAL
)

func (l LogLevel) FromStr(s string) {
	s = strings.ToLower(s)
	switch s {
	case "trace":
		l = TRACE
	case "debug":
		l = DEBUG
	case "info":
		l = INFO
	case "warn":
		l = WARN
	case "error":
		l = ERROR
	case "fatal":
		l = FATAL
	}
}

func (l LogLevel) ToStr() string {
	switch l {
	case TRACE:
		return "TRACE"
	case DEBUG:
		return "DEBUG"
	case INFO:
		return "INFO"
	case WARN:
		return "WARN"
	case ERROR:
		return "ERROR"
	case FATAL:
		return "FATAL"
	}

	return "UNDEF"
}

// Struttura per una entry del log. La chiave nel db è la
// data/ora UTC dell'evento , in formato RFC3339 modificato
// per garantire l'ordinabilità delle chiavi
//
// time.Format("2006-01-02T15:04:05.000000Z07:00")
type LogEntry struct {
	Time   time.Time         `json:"-"`
	Level  LogLevel          `json:"level"`
	Msg    string            `json:"message"`
	Values map[string]string `json:"values"`
}

// Struttura composta da una LogEntry e la relativa chiave nel
// DB. Dato che le maps in Go non hanno ordinamento, utilizzo
// questa struttura quando recupero i dati dal database, in modo
// da poter formare un array ordinabile senza perdere il valore
// della chiave nel DB.
type DBEntry struct {
	LogEntry
	Key string
}
