package models

type InstanceConf struct {
	ID       int    `db:"instance_id"`
	Name     string `db:"name"`
	ExecPath string `db:"exec_path"`
	Args     string `db:"args"`

	MaxRetries int // forse?
	RetrySleep int // forse?

	ProductName     string `db:"product_name"`
	UpdateProcedure string `db:"procedure"`
}

type CachableConf struct {
	InstancesConf []InstanceConf
}
