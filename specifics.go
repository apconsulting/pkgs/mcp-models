package models

import "time"

// Info sui moduli AP4B installati
type Module struct {
	Description string    `json:"description"`
	VersionTag  string    `json:"version"`
	DateUpdate  time.Time `json:"date-update"`
}
