package models

import "time"

const MCP_PREFIX = "[MCP-PREFIX]"

// Informazioni di sistema della macchina che ospita il client
type SystemInfo struct {
	Hostname string `json:"hostname"`
	Platform string `json:"platform"`
	Cpu      string `json:"cpu"`
	Ram      uint64 `json:"ram"`
	Disk     uint64 `json:"disk"`
}

const (
	STATUS_RUNNING = "Running"
	STATUS_STOPPED = "Stopped"
)

// Struttura dati in ingresso dai client
type ClientInfo struct {
	Description string         `json:"company-name"`
	VersionTag  string         `json:"main-version"`
	IPAddress   string         `json:"ip-addr"`
	Status      string         `json:"status"`
	System      SystemInfo     `json:"system,omitempty"`
	Vars        map[string]any `json:"vars,omitempty"`
}

// Status esteso del client, include informazioni che non vengono
// comunicate dai client stessi ma che devono persistere in DB
// (ad es. la versione schedulata per il prossimo aggiornamento)
type ClientStatus struct {
	ClientInfo `json:"client-info"`

	UpgradeTo   string     `json:"upgrade-to,omitempty"`
	UpgradeTime *time.Time `json:"scheduled-time,omitempty"`
	IsUpgrading bool       `json:"is-upgrading"` // TRUE se client ha pingato e ricevuto ordine di schedulazione
	JwtToken    string     `json:"jwt-token"`
	LastSeen    time.Time  `json:"last-seen"`

	LogoURL string `json:"logo-url"`
}

// Oggetto che rappresenta un client in Application Logic, utile per
// decorare lo Status con informazioni importanti, ad esempio per la
// corretta presentazione dei dati nel template, ma che non devono
// persistere in database (i.e. la validità del token JWT, che
// dipende solamente dalla blocklist)
type Client struct {
	ClientStatus

	Key           string
	JwtValidToken bool
	LogEntries    []DBEntry
}
